<?php 

class Categories extends CI_Controller{

	public function __construct(){
		parent::__construct();

		if($this->session->userdata('user_name') == ''){
			redirect('Logins');
		}

		$this->load->model(array('Category'));
	}

	public function index(){
		$data = array();
		$data['title'] = "Category List";
		$data['headline'] = "Manage Category/ Category";
		$data['category_list'] = $this->Category->get_list();

		$this->load->view('Categories/index',$data);
	}

	public function add(){

		$this->prepare_validation();

		if($_POST){
			$data = $this->get_posted_data();
			if($this->form_validation->run()== TRUE){
				if($this->Category->add($data)){
					$this->session->set_flashdata('success',ADD_MESSAGE);

				}else{
					$this->session->set_flashdata('warning',WARNING_MESSAGE);
				}

				redirect('Categories/index');

			}else{
                 redirect('Categories');
			}
		}

	}


	public function edit(){
		$id = $this->input->post('id');
		$callback_message = array();

		$get_category_info = $this->Category->get_category_by_id($id);
		foreach($get_category_info as $info){
			$callback_message['category_name'] = $info->category_name;
			$callback_message['id'] = $info->id;
		}

		echo json_encode($callback_message);

	}

	public function update(){
		if(isset($_POST)){
			$data = $this->get_posted_data();
			$data['id'] = $this->input->post('id');

			if($this->Category->update($data)){
				$this->session->set_flashdata('success', EDIT_MESSAGE);
			}else{
				$this->session->set_flashdata('warning', WARNING_MESSAGE);
			}

			redirect('Categories/index');
		}
	}

	public function delete($id){
		if(!empty($id)){
			if($this->Category->delete($id)){
			$this->session->set_flashdata('success', DELETE_MESSAGE);
		}else{
			$this->session->set_flashdata('warning', WARNING_MESSAGE);
		}
		redirect('Categories/index');
	}else{
		redirect('Categories');
	}
		

	}



	public function get_posted_data(){
		$data = array();
		$data['category_name'] = $this->input->post('category_name');
		return $data;
	}

	public function prepare_validation(){
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">','</div>');
		$this->form_validation->set_rules('category_name','category_name', 'trim|required');
	}


}