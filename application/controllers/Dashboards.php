<?php 

class Dashboards extends CI_Controller{

	public function __construct(){
		parent::__construct();

		if($this->session->userdata('user_name') == ''){
			redirect('Logins');
		}

		$this->load->model(array('Dashboard','Category','Post'));
	}

	public function index(){
		$data = array();
		$data['title'] = "Dashboard";
		$data['headline'] = "Dashboard/ Dashboard";
		$data['dashboard_info'] = $this->Dashboard->count_info();

		$data['chart_data'] = $this->Dashboard->get_chart_data();
		$data['chart_data_for_user'] = $this->Dashboard->get_chart_data_for_user();
		

		$this->load->view('Dashboards/index',$data);
	}
}