<?php 

class Homes extends CI_Controller{

	public function __construct(){
		parent::__construct();

		$this->load->model(array('Home','Category','Post'));
	}

	public function index(){
		$data = array();

		$data['category_list'] = $this->Home->category_list();
		$data['is_feature'] = $this->Home->is_feature_list();
		$data['is_feature_other_list'] = $this->Home->is_feature_other_list();
		$data['international'] = $this->Home->get_international_news_list();
		$data['bangladesh'] = $this->Home->get_bangladesh_news_list();
		$data['politics'] = $this->Home->get_politics_news_list();
		$data['sports'] = $this->Home->get_sports_news_list();
		
		$data['middle_add'] = $this->Home->get_middle_ad();
		$data['bottom_add'] = $this->Home->get_bottom_ad();
		$data['top_add'] = $this->Home->get_top_ad();
		$data['logo'] = $this->Home->get_logo();

		// echo "<pre>";print_r($data);die;
		$this->load->view('website/index',$data);
	}


	public function show_news_list($id){
		$data = array();
		$data['category_list'] = $this->Home->category_list();
		$data['is_feature'] = $this->Home->is_feature_list();
		$data['category_name'] = $this->Home->category_name_by_id($id);
		$data['post_list'] = $this->Home->post_list_by_category_id($id);
		$data['post_other_list'] = $this->Home->post_other_list_by_category_id($id);
		$data['top_add'] = $this->Home->get_top_ad();
		$data['logo'] = $this->Home->get_logo();
		// echo "<pre>";print_r($data);die;
		$this->load->view('website/news_list',$data);
	}


	public function show_news_details($id){
		$data = array();
		$data['category_list'] = $this->Home->category_list();
		$data['category_name'] = $this->Home->category_name_by_post_id($id);
		$data['top_add'] = $this->Home->get_top_ad();
		$data['logo'] = $this->Home->get_logo();
		
		$data['specific_post'] = $this->Home->get_post_by_id($id);
		$data['category_wise_post'] = $this->Home->category_wise_post($id);
		 // echo "<pre>";print_r($data);die;
		 

		$this->load->view('website/news_details',$data);
	}

}
