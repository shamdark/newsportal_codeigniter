<?php 

class Logins extends CI_Controller{

	public function __construct(){
		parent::__construct();

		$this->load->model(array('Login'));
	}

	public function index(){
		$this->load->view('Logins/index');
	}

	public function validate(){
		$this->prepare_validation();

		if(isset($_POST)){
			$data = $this->get_data();
			if($this->form_validation->run() == TRUE){
				$validate_user = $this->Login->check_user($data);
				$session_data = array(
					'user_id'=>$validate_user->id,
					'user_name'=>$validate_user->user_name,
					'role_id'=>$validate_user->role_id
				);
				$this->session->set_userdata($session_data);
				redirect('Dashboards');
				
			}
			else{
				$this->load->view('Logins/index',$data);

			}
		}

	}


	public function get_data(){
		$data = array();

		$data['user_name'] = $this->input->post('user_name');
		$data['password'] = $this->input->post('password');

		return $data;
	}

	public function prepare_validation(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('user_name','user_name','trim|required');
		$this->form_validation->set_rules('password','password','trim|required');
	}


	public function logout(){
		$this->session->sess_destroy();
		redirect('Logins');
	}




}