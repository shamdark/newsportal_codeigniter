<?php 

class Logos extends CI_Controller{

	public function __construct(){
		parent::__construct();

		if($this->session->userdata('user_name') == ''){
			redirect('Logins');
		}

		$this->load->model(array('Logo'));
	}

	public function index(){
		$data = array();
		$data['title'] = "Logo & Bannar List";
		$data['headline'] = "Manage Logo & Bannar/ Logo & Bannar";
		$data['logo_list'] = $this->Logo->get_list();
		$this->load->view('Logos/index',$data);
	}

	public function add(){

		$data = $this->get_posted_data();
		$data['created_at'] = date("Y/m/d");
		$image_name = $this->do_upload();
         	if(!empty($image_name)){
         		$data['image'] = $image_name;
         	}
         	$data['created_at'] = date("Y/m/d");
         	if($this->Logo->add($data)){
				$this->session->set_flashdata('success',ADD_MESSAGE);

			}else{
				$this->session->set_flashdata('warning',WARNING_MESSAGE);
			}

		redirect('Logos/index');

	}


	public function edit($id=null){
		$data = array();

        if(isset($_POST) && !empty($_POST)){
        	$data = $this->get_posted_data();
        	$data['id'] = $this->input->post('id');
        	$data['updated_at'] = date("Y/m/d");

        	$image_name = $this->do_upload();
        	
        	if(!empty($image_name)){
         		$data['image'] = $image_name;
         	}

        	if($this->Logo->edit($data)){
				$this->session->set_flashdata('success',EDIT_MESSAGE);

			}else{
				$this->session->set_flashdata('warning',WARNING_MESSAGE);
			}

			redirect('Logos/index');

        	
        }

        if(empty($id) || $id == ""){
            $this->session->set_flashdata('warning', WARNING_MESSAGE);
            redirect('Logos/index', 'refresh');
        }
		
		$data['get_logo_info'] = $this->Logo->get_logo_by_id($id);
		$data['title'] = "Edit Logo/Ad";
		$data['headline'] = "Manage Logo/ Ad";

        // echo "<pre>";print_r($data);die;
		$this->load->view('Logos/edit',$data);
	}


	public function delete($id,$url){

    	if(empty($id) || $id == ""){
            $this->session->set_flashdata('warning', WARNING_MESSAGE);
            redirect('Logos/index', 'refresh');
        }

		if($this->Logo->delete($id)){
			if(isset($url)){
                $url="assets/images/".$url;
                unlink($url);
            }

			$this->session->set_flashdata('success', DELETE_MESSAGE);
		}else{
			$this->session->set_flashdata('warning', WARNING_MESSAGE);
		}
		redirect('Logos/index');
	
		

	}


	 private function do_upload(){

        $type=explode('.',$_FILES['image']['name']);
        $type=$type[count($type)-1];
        $image_name = uniqid(rand()).'.'.$type;
        $url="assets/images/".$image_name;
        if(in_array($type,array('jpg','JPG','JPEG','jpeg','png','PNG','bmp','BMP','pdf')))
        {
            if(move_uploaded_file($_FILES['image']['tmp_name'],$url))
            {
                return $image_name;
            }
        }
    }


	public function get_posted_data(){
		$data = array();
		$user_id = $this->session->userdata();
		$data['position'] = $this->input->post('position');
		$data['type'] = $this->input->post('type');
		$data['user_id'] = $user_id['user_id'];
		

		return $data;
	}
}