<?php 

class Posts extends CI_Controller{

	public function __construct(){
		parent::__construct();

		if($this->session->userdata('user_name') == ''){
			redirect('Logins');
		}

		$this->load->model(array('Post','Category'));
	}

	public function index(){
		$data = array();
		$data['title'] = "Post List";
		$data['headline'] = "Manage Posty/ Post";
		$data['post_list'] = $this->Post->get_list();

		$this->load->view('Posts/index',$data);
	}

	public function add(){
		$data = array();

		if(isset($_POST) && !empty($_POST)){
		 $this->prepare_validation();
         $data = $this->get_posted_data();
         if($this->form_validation->run()== TRUE){
         	$image_name = $this->do_upload();
         	if(!empty($image_name)){
         		$data['image'] = $image_name;
         	}
         	$data['created_at'] = date("Y/m/d");
         	if($this->Post->add($data)){
				$this->session->set_flashdata('success',ADD_MESSAGE);

			}else{
				$this->session->set_flashdata('warning',WARNING_MESSAGE);
			}

			 redirect('Posts/index');
         }else{
         	$this->session->set_flashdata('warning',WARNING_MESSAGE);
         	$this->load->view('Posts/add');
         }

         
		}
		$data['category_list'] = $this->Category->get_list();
		$data['title'] = "Add Post";
		$data['headline'] = "Manage Post/Add Post";
		$this->load->view('Posts/add',$data);
	}


	public function edit($id=null){
		$data = array();

        if(isset($_POST) && !empty($_POST)){
        	$data = $this->get_posted_data();
        	$data['id'] = $this->input->post('id');
        	$data['updated_at'] = date("Y/m/d");

        	$image_name = $this->do_upload();
        	
        	if(!empty($image_name)){
         		$data['image'] = $image_name;
         	}

        	if($this->Post->edit($data)){
				$this->session->set_flashdata('success',EDIT_MESSAGE);

			}else{
				$this->session->set_flashdata('warning',WARNING_MESSAGE);
			}

			redirect('Posts/index');

        	
        }

        if(empty($id) || $id == ""){
            $this->session->set_flashdata('warning', WARNING_MESSAGE);
            redirect('Posts/index', 'refresh');
        }
		$data['category_list'] = $this->Category->get_list();
		$data['get_post_info'] = $this->Post->get_post_by_id($id);
		$data['title'] = "Edit Post";
		$data['headline'] = "Manage Post/Edit Post";


		$this->load->view('Posts/edit',$data);
	}
	


	 private function do_upload(){

        $type=explode('.',$_FILES['image']['name']);
        $type=$type[count($type)-1];
        $image_name = uniqid(rand()).'.'.$type;
        $url="assets/images/".$image_name;
        if(in_array($type,array('jpg','JPG','JPEG','jpeg','png','PNG','bmp','BMP','pdf')))
        {
            if(move_uploaded_file($_FILES['image']['tmp_name'],$url))
            {
                return $image_name;
            }
        }
    }



    public function delete($id,$url){

    	if(empty($id) || $id == ""){
            $this->session->set_flashdata('warning', WARNING_MESSAGE);
            redirect('Posts/index', 'refresh');
        }

		if($this->Post->delete($id)){
			if(isset($url)){
                $url="assets/images/".$url;
                unlink($url);
            }

			$this->session->set_flashdata('success', DELETE_MESSAGE);
		}else{
			$this->session->set_flashdata('warning', WARNING_MESSAGE);
		}
		redirect('Posts/index');
	
		

	}


	public function get_posted_data(){
		$data = array();
		$user_id = $this->session->userdata();
        

		$data['title'] = $this->input->post('title');
		$data['description'] = trim($this->input->post('description'));
		$data['category_id'] = $this->input->post('category_id');
		$data['is_feature'] = $this->input->post('is_feature');
		$data['entry_by'] = $user_id['user_id'];

		return $data;
	}

	public function prepare_validation(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title','title','trim|required');
		$this->form_validation->set_rules('description','description','trim|required');
		$this->form_validation->set_rules('category_id','category_id','trim|required');
		
	}
}