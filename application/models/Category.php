<?php 

class Category extends CI_Model{

	public function get_list(){

		$query = $this->db->query("SELECT * FROM category ORDER BY id DESC")->result();
		return $query;
	}

	public function add($data){
		return $this->db->insert('category', $data);
	}

	public function get_category_by_id($id){
		if(!empty($id)){
			$query = $this->db->query("SELECT * FROM category WHERE id=$id")->result();
			return $query;
		}
	}

	public function update($data){
		$this->db->where('id', $data['id']);
		return $this->db->update('category',$data);
	}

	public function delete($id){
		$this->db->where('id',$id);
		return $this->db->delete('category');
	}
}