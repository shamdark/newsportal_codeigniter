<?php 

class Dashboard extends CI_Model{

 public function count_info(){
 	$data['total_cat'] = $this->db->query("SELECT COUNT(id) as total_category FROM category")->row();
 	$data['total_user'] = $this->db->query("SELECT COUNT(id) as total_user FROM user")->row();
 	$data['total_post'] = $this->db->query("SELECT COUNT(id) as total_post FROM post")->row();

 	return $data;

 }

 public function get_chart_data(){
 	$query = $this->db->query("SELECT COUNT(post.id) AS total_post,category.category_name FROM post JOIN category ON post.category_id = category.id  GROUP BY post.category_id")->result();

 	return $query;
 }

 public function get_chart_data_for_user(){
 	$query = $this->db->query("SELECT COUNT(post.id) AS total_post,user.user_name FROM post JOIN user ON post.entry_by = user.id  GROUP BY post.entry_by")->result();

 	return $query;
 }
}