<?php 

class Home extends CI_Model{


	public function category_list(){
		$query = $this->db->query("SELECT * FROM category ORDER BY id DESC")->result();
		return $query;
	}

	public function is_feature_list(){
		$query = $this->db->query(" SELECT id,title,image FROM post WHERE is_feature = 1 ORDER BY id DESC limit 3")->result();
		return $query;
	}

	public function is_feature_other_list(){
		$query = $this->db->query("SELECT id,title,image,created_at FROM post WHERE is_feature = 1 ORDER BY id DESC limit 3 OFFSET 3")->result();
		return $query;
	}

	public function get_international_news_list(){
		$query = $this->db->query("SELECT post.id,post.title,post.image,post.created_at,category.category_name FROM post JOIN category ON post.category_id = category.id WHERE category.category_name = 'international' ORDER BY post.id DESC limit 4 ")->result();
		return $query;
	}
	public function get_bangladesh_news_list(){
		$query = $this->db->query("SELECT post.id,post.title,post.image,post.created_at,category.category_name FROM post JOIN category ON post.category_id = category.id WHERE category.category_name = 'bangladesh' ORDER BY post.id DESC limit 3 ")->result();
		return $query;
	}

	public function get_politics_news_list(){
		$query = $this->db->query("SELECT post.id,post.title,post.image,post.created_at,category.category_name FROM post JOIN category ON post.category_id = category.id WHERE category.category_name = 'politics' ORDER BY post.id DESC limit 3 ")->result();
		return $query;
	}

	public function get_sports_news_list(){
		$query = $this->db->query("SELECT post.id,post.title,post.image,post.created_at,category.category_name FROM post JOIN category ON post.category_id = category.id WHERE category.category_name = 'sports' ORDER BY post.id DESC limit 3 ")->result();
		return $query;
	}

	public function category_name_by_id($id){
		$query = $this->db->query("SELECT category_name FROM category WHERE id=$id ")->row()->category_name;
		return $query;
	}

	public function post_list_by_category_id($id){
		$query = $this->db->query("SELECT post.id,post.title,post.image,post.created_at,category.category_name FROM post JOIN category ON post.category_id = category.id WHERE post.category_id = $id ORDER BY post.id DESC ")->result();
		return $query;
	}

	public function post_other_list_by_category_id($id){
		$query = $this->db->query("SELECT post.id,post.title,post.image,post.created_at,category.category_name FROM post JOIN category ON post.category_id = category.id WHERE post.category_id = $id ORDER BY post.id DESC Limit 30 OFFSET 30 ")->result();
		return $query;
	}

	public function get_post_by_id($id){
		$query = $this->db->query("SELECT post.id,post.title,user.user_name,post.image,post.created_at,post.description,category.category_name FROM post JOIN category ON post.category_id = category.id JOIN user ON post.entry_by = user.id WHERE post.id = $id ")->row();
		return $query;
	}

	public function category_wise_post($id){
		$query = $this->db->query("SELECT post.category_id as cat_id FROM post WHERE post.id = $id ")->row()->cat_id;

		$get_data = $this->db->query("SELECT post.category_id,post.id,post.title,post.image,post.created_at,category.category_name FROM post JOIN category ON post.category_id = category.id WHERE post.category_id = $query AND post.id <> $id ")->result();
		
		return $get_data;
	}

	public function category_name_by_post_id($id){
		$query = $this->db->query("SELECT category.category_name FROM post JOIN category ON post.category_id = category.id WHERE post.id=$id ")->row()->category_name;
		return $query;
	}

	public function get_top_ad(){
		$query = $this->db->query("SELECT logos.image FROM logos WHERE logos.position = 1 AND logos.type=0 ORDER BY logos.id DESC Limit 1 ")->row()->image;
		
		return $query;
	}
	public function get_middle_ad(){
		$query = $this->db->query("SELECT logos.image FROM logos WHERE logos.position = 2 AND logos.type=0 ORDER BY logos.id DESC Limit 1 ")->row()->image;
		return $query;
	}

	public function get_bottom_ad(){
		$query = $this->db->query("SELECT logos.image FROM logos WHERE logos.position = 3 AND logos.type=0 ORDER BY logos.id DESC Limit 1 ")->row()->image;
		return $query;
	}

	public function get_logo(){
		$query = $this->db->query("SELECT logos.image FROM logos WHERE logos.type=1 ORDER BY logos.id DESC Limit 1 ")->row()->image;
		return $query;
	}

 }
