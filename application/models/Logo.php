<?php 

class Logo extends CI_Model{

	public function add($data){
		return $this->db->insert('logos', $data);
	}

	public function get_list(){
		$query = $this->db->query("SELECT logos.*,user.user_name FROM logos JOIN user ON logos.user_id = user.id ORDER BY logos.id DESC")->result();

	
		return $query;
	}

	public function get_logo_by_id($id){
		$query = $this->db->query("SELECT logos.* FROM logos  WHERE logos.id=$id")->row();	
		return $query;
	}

	public function edit($data){

		$this->db->where('id', $data['id']);
		return $this->db->update('logos',$data);
	}

	public function delete($id){
		$this->db->where('id',$id);
		return $this->db->delete('logos');
	}

}