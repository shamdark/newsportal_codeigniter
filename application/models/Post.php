<?php 

class Post extends CI_Model{

	public function get_list(){
		$query = $this->db->query("SELECT post.*,category.category_name,user.user_name FROM post JOIN category ON post.category_id = category.id JOIN user ON post.entry_by = user.id ORDER BY post.id DESC")->result();

	
		return $query;
	}

	public function add($data){

		return $this->db->insert('post', $data);
	}

	public function delete($id){
		$this->db->where('id',$id);
		return $this->db->delete('post');
	}

	public function get_post_by_id($id){
		$query = $this->db->query("SELECT post.*,category.category_name,user.user_name FROM post JOIN category ON post.category_id = category.id JOIN user ON post.entry_by = user.id WHERE post.id=$id")->row();

	
		return $query;
	}

	public function edit($data){

		$this->db->where('id', $data['id']);
		return $this->db->update('post',$data);
	}
}