<?php $this->load->view('Layouts/admin_header');?>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row justify-content-center">
          <div class="col-12 col-sm-6 col-md-10">

          	<!------------add category modal start--------------------------->

          	<!-- Modal -->
			<div id="add_category" class="modal fade" >
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Add Category </h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			       <form action="<?php echo base_url();?>index.php/Categories/add" method="post">
			       	<div class="form-group">
				    <label for="exampleInputPassword1">Category Name</label>
				    <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Category Name">
				   </div>
			       
			      </div>
			      <div class="modal-footer">
			      
			        <input type="submit" class="btn btn-primary" value="Save">
			      </div>
			      </form>
			    </div>
			  </div>
			</div>
          	<!------------add category modal end----------------------------->


         <!------------edit category modal start--------------------------->

          	<!-- Modal -->
			<div id="edit_category" class="modal fade" >
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Update Category</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			       <form action="<?php echo base_url();?>index.php/Categories/update" method="post">
			       	<div class="form-group">

			       	<input type="hidden" name="id" id="id" >
			       	<?php echo form_error('category_name');?>
				    <label for="exampleInputPassword1">Category Name</label>
				    <input type="text" class="form-control" required="required" id="edit_category_name" name="category_name" placeholder="Category Name">
				   </div>
			       
			      </div>
			      <div class="modal-footer">
			      
			        <input type="submit" class="btn btn-primary" value="Save">
			      </div>
			      </form>
			    </div>
			  </div>
			</div>
          	<!------------edit category modal end----------------------------->

           <div class="card">

           	 <div class="card-header">
                <h3 class="card-title">Category </h3>
                <button type="button" class="add_category btn btn-sm btn-primary" style="float: right;">Add</button>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <table id="cat_table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>SL</th>
                    <th>Category Name</th>
                    <th>Action</th>
                    
                  </tr>
                  </thead>
                  <tbody>
                 <?php if(isset($category_list) && !empty($category_list)){ 
                 	$i=1;
                 	foreach($category_list as $list){ ?>

                  <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $list->category_name;?></td>
                    <td>
                    	<a  class="edit_category btn btn-info btn-xs" id="<?php echo $list->id;?>"> Edit</a>
                    	<a href="<?php echo base_url();?>index.php/Categories/delete/<?php echo $list->id;?>" class="btn btn-danger btn-xs"> Delete</a>
                    </td>
                  </tr>
                  
                  <?php $i++; } }?>
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
         
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
 <?php $this->load->view('Layouts/admin_footer');?>