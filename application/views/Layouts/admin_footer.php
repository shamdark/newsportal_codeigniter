 </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="http:thejournalexpress24.com/portfolio">Ahatasham</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.5
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url();?>assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="<?php echo base_url();?>assets/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="<?php echo base_url();?>assets/plugins/raphael/raphael.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url();?>assets/plugins/chart.js/Chart.min.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<!-- PAGE SCRIPTS -->
<script src="<?php echo base_url();?>assets/dist/js/pages/dashboard2.js"></script>
<script src="<?php echo base_url();?>assets/plugins/summernote/summernote-bs4.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
      google.charts.load('visualization', "1", {
          packages: ['corechart']
      });
  </script>

  <script language="JavaScript">
  // Draw the Pie chart
  google.charts.setOnLoadCallback(pieChart);  
  //for month wise
  function pieChart() {
        /* Define the chart to be drawn.*/
        var data = google.visualization.arrayToDataTable([
            ['Category', 'Count Post'],
            <?php
             foreach ($chart_data as $row){
             echo "['".$row->category_name."',".$row->total_post."],";
             }
             ?>
        ]);
        var options = {
            title: 'Category Wise Post Count',
            is3D: true,
        };
        /* Instantiate and draw the chart.*/
        var chart = new google.visualization.PieChart(document.getElementById('pie_chart'));
        chart.draw(data, options);
  }
</script>

  <script language="JavaScript">
  // Draw the Pie chart
  google.charts.setOnLoadCallback(pieChart);  
  //for month wise
  function pieChart() {
        /* Define the chart to be drawn.*/
        var data = google.visualization.arrayToDataTable([
            ['User', 'Count Post'],
            <?php
             foreach ($chart_data_for_user as $row){
             echo "['".$row->user_name."',".$row->total_post."],";
             }
             ?>
        ]);
        var options = {
            title: 'User Wise Post Count',
            is3D: true,
        };
        /* Instantiate and draw the chart.*/
        var chart = new google.visualization.PieChart(document.getElementById('user_chart'));
        chart.draw(data, options);
  }
</script>


<script>
  $("document").ready(function(){
    $(document).on('click', '.add_category', function(){
      $("#add_category").modal('show');
    });


    $(document).on('click', '.edit_category', function(){
       var id = $(this).attr("id");
       $.post("<?php echo base_url();?>index.php/Categories/edit", {id: id},
        function(data){
          $("#edit_category_name").val(data.category_name);
          $("#id").val(data.id);

        }, "json");
       $("#edit_category").modal('show');
    });


    $(document).on('click', '.add_logos', function(){
      $("#add_logos").modal('show');
    });


    $(document).on('click', '.edit_logos', function(){
      var base_url = "<?php echo base_url();?>assets/images/";
       var id = $(this).attr("id");
       $.post("<?php echo base_url();?>index.php/Logos/edit", {id: id},
        function(data){
          $("#image_view").html("<img src= data[0].image >");;
          $("#id").val(data.id);
          $("#position").val(data.position);
          $("#type").val(data.type);

        }, "json");
       $("#edit_logos").modal('show');
    });

  });


</script>

<script>
  $(function () {
    $("#cat_table").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
   
  });
</script>

<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>

</body>
</html>
