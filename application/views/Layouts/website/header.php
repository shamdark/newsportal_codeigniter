<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<!--title-->
    <title>Newsportal</title>
	
	<!--CSS-->
    <link href="<?php echo base_url();?>assets/website/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/website/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/website/css/magnific-popup.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/website/css/owl.carousel.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/website/css/subscribe-better.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/website/css/main.css" rel="stylesheet">
	<link id="preset" rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/website/css/presets/preset1.css">
	<link href="<?php echo base_url();?>assets/website/css/responsive.css" rel="stylesheet">		
	<link href="<?php echo base_url();?>assets/dist/css/custom.css" rel="stylesheet">		
	
	<!--Google Fonts-->
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>
	
    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">


</head><!--/head-->
<body>
	<div id="main-wrapper" class="homepage-two fixed-nav">
		<div class="topbar">
			<div class="container">
				<div id="date-time"></div>
				<div id="topbar-right">
					<div class="dropdown language-dropdown">						
						<a data-toggle="dropdown" href="#"><span class="change-text">En</span> <i class="fa fa-angle-down"></i></a>
						<ul class="dropdown-menu language-change">
							<li><a href="#">EN</a></li>
							<li><a href="#">FR</a></li>
							<li><a href="#">GR</a></li>
							<li><a href="#">ES</a></li>
						</ul>								
					</div>				
					<div id="weather"></div>
					<div class="searchNlogin">
						<ul>
							<li class="search-icon"><i class="fa fa-search"></i></li>
							<li class="dropdown user-panel"><a href="<?php echo base_url();?>index.php/Logins" ><i class="fa fa-user"></i></a>
								
							</li>
						</ul>
						<div class="search">
							<form role="form">
								<input type="text" class="search-form" autocomplete="off" placeholder="Type & Press Enter">
							</form>
						</div> <!--/.search--> 
					</div><!-- searchNlogin -->
				</div>
			</div>
		</div>
		<div id="navigation">
			<div class="navbar" role="banner">
				<div class="container">
					<div class="top-add">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							
							<a class="navbar-brand" href="<?php echo base_url();?>index.php/Homes">
								<?php if(isset($logo) && !empty($logo)){ ?>
								<img class="img-responsive" src="<?php echo base_url();?>assets/images/<?php echo $logo;?>" alt="" height="40" width="150" />
							<?php } ?>
							</a>
						</div> 
						<div class="navbar-right">
							<a href="<?php echo base_url();?>index.php/Homes">
								<?php if(isset($top_add) && !empty($top_add)){ ?>
								<img class="img-responsive" src="<?php echo base_url();?>assets/images/<?php echo $top_add;?>" alt="" width="1171" height="89" />
							<?php } ?>
							</a>
						</div>
					</div>
				</div> 
				<div id="menubar">
					<div class="container">
						<nav id="mainmenu" class="navbar-left collapse navbar-collapse"> 
							<ul class="nav navbar-nav">                       
							<li class="home dropdown"><a href="<?php echo base_url();?>index.php/Homes" >Home</a>
								
							</li>

							<?php if(isset($category_list) && !empty($category_list)){
								foreach($category_list as $menu){ ?>

								<li class="home dropdown"><a href="<?php echo base_url();?>index.php/Homes/show_news_list/<?php echo $menu->id;?>" ><?php echo $menu->category_name;?> </a>
								 </li>

								<?php } } ?>

							
							
							
						</ul> 									
						</nav>					
					</div>
				</div><!--/#navigation--> 
			</div><!--/#navigation--> 
		</div><!--/#navigation--> 