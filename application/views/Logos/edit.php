<?php $this->load->view('Layouts/admin_header');?>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row justify-content-center">
          <div class="col-12 col-sm-6 col-md-6">

           <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Select2 (Default Theme)</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
          	  <form action="<?php echo base_url();?>index.php/Logos/edit" method="post" enctype='multipart/form-data'>
			       	<div class="form-group">
				    <label for="exampleInputPassword1">Logo/Ad (Size should be 1171 X 89 for ad only)</label>
				    <input type="file" class="form-control"  name="image" multiple="multiple">
				    <div class="image"> <img src="<?php if(isset($get_logo_info) && !empty($get_logo_info->image)){?> <?php echo base_url();?>assets/images/<?php echo $get_logo_info->image;?> <?php } ?>" width="150" height="100"></div>
				   </div>

				   <input type="hidden" name="id" value="<?php echo $get_logo_info->id;?>">

				  <div class="form-group">
	                  <label>Upload Type </label>
	                  <select class="form-control select2" name="type" style="width: 100%;">
	                    <option value ="0" <?php if(isset($get_logo_info) && !empty($get_logo_info->type == 0)){?>selected="selected" <?php } ?>>Ad</option>
	                    <option value="1" <?php if(isset($get_logo_info) && !empty($get_logo_info->type == 1)){?>selected="selected" <?php } ?>>Logo</option>
	                    
	                    
	                  </select>
                 </div>


                  <div class="form-group">
	                  <label>Upload position for ad</label>
	                  <select class="form-control select2" name="position" style="width: 100%;">
	                    <option value ="">---Select position for ad ---</option>
	                    <option value ="1" <?php if(isset($get_logo_info) && !empty($get_logo_info->position == 1)){?>selected="selected" <?php } ?>>top of the page</option>
	                    <option value="2" <?php if(isset($get_logo_info) && !empty($get_logo_info->position == 2)){?>selected="selected" <?php } ?>>middle of the page</option>
	                    <option value="3" <?php if(isset($get_logo_info) && !empty($get_logo_info->position == 3)){?>selected="selected" <?php } ?>>bottom bottom of the page</option>
	                    
	                    
	                  </select>
                 </div>
			       
			      </div>
			      <div class="modal-footer">
			      
			        <input type="submit" class="btn btn-primary" value="Save">
			      </div>
			      </form>
            <!-- /.row -->


          </div>
          <!-- /.card-body -->
          
        </div>
        <!-- /.card -->
          </div>
         
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
 <?php $this->load->view('Layouts/admin_footer');?>