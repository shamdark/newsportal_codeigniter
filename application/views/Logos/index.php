<?php $this->load->view('Layouts/admin_header');?>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row justify-content-center">
          <div class="col-12 col-sm-6 col-md-12">

          	<!------------add Logos modal start--------------------------->

          	<!-- Modal -->
			<div id="add_logos" class="modal fade" >
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Add Logo/Ad  </h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			       <form action="<?php echo base_url();?>index.php/Logos/add" method="post" enctype='multipart/form-data'>
			       	<div class="form-group">
				    <label for="exampleInputPassword1">Logo/Ad (Size should be 1171 X 89 for ad only)</label>
				    <input type="file" class="form-control"  name="image" multiple="multiple">
				   </div>

				  <div class="form-group">
	                  <label>Upload Type </label>
	                  <select class="form-control select2" name="type" style="width: 100%;">
	                    <option value ="0" selected="selected">Ad</option>
	                    <option value="1">Logo</option>
	                    
	                    
	                  </select>
                 </div>


                  <div class="form-group">
	                  <label>Upload position for ad</label>
	                  <select class="form-control select2" name="position" style="width: 100%;">
	                    <option value ="">---Select position for ad ---</option>
	                    <option value ="1">top of the page</option>
	                    <option value="2">middle of the page</option>
	                    <option value="3">bottom bottom of the page</option>
	                    
	                    
	                  </select>
                 </div>
			       
			      </div>
			      <div class="modal-footer">
			      
			        <input type="submit" class="btn btn-primary" value="Save">
			      </div>
			      </form>
			    </div>
			  </div>
			</div>
          	<!------------add Logos modal end----------------------------->

           <div class="card">

           	 <div class="card-header">
                <h3 class="card-title">DataTable with default features</h3>
                 <button type="button" class="add_logos btn btn-sm btn-primary" style="float: right;">Add</button>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <table id="cat_table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>SL</th>
                    <th>Image </th>
                    <th>Image Type </th>
                    <th>Entry By </th>
                    <th>Action</th>
                    
                  </tr>
                  </thead>
                  <tbody>
                 <?php if(isset($logo_list) && !empty($logo_list)){ 
                 	$i=1;
                 	foreach($logo_list as $list){ ?>

                  <tr>
                    <td><?php echo $i;?></td>
                    <td><img src="<?php echo base_url();?>assets/images/<?php echo $list->image;?>" width="50" height="50"></td>
                    <td><?php if($list->type == 1){echo "Logo" ;} else{ echo "Ad" ;} ?></td>
                    <td>
                    	<?php if($list->position == 1){echo "Top of the page" ;}
                        elseif($list->position == 2){ echo "Middle of the page" ;}
                        elseif($list->position == 3){ echo "Bottom of the page" ;}
                        else{ } ?>
                     </td>
                    
                    <td><?php echo $list->user_name; ?></td>
                    <td>
                    	<a href="<?php echo base_url();?>index.php/Logos/edit/<?php echo $list->id;?>" class=" btn btn-info btn-sm" > Edit</a>
                    	<a href="<?php echo base_url();?>index.php/Logos/delete/<?php echo $list->id;?>/<?php echo $list->image;?>" class="btn btn-danger btn-sm"> Delete</a>
                    </td>
                  </tr>
                  
                  <?php $i++; } }?>
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
         
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
 <?php $this->load->view('Layouts/admin_footer');?>