<?php $this->load->view('Layouts/admin_header');?>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row justify-content-center">
          <div class="col-12 col-sm-6 col-md-12">

           <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Select2 (Default Theme)</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
          	<form method="post" action="<?php echo base_url();?>index.php/Posts/edit" enctype='multipart/form-data'>
            <div class="row">

            	<input type = "hidden" name="id" value="<?php echo $get_post_info->id;?>">
            
              <div class="col-md-8">
                <div class="form-group">

                  <label>Title</label>
                   <input type="text" class="form-control"  name="title" value="<?php if(isset($get_post_info)){ echo $get_post_info->title;} ?>">
                   
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Description</label>
                  <textarea class="textarea" name="description" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                          <?php if(isset($get_post_info)){ echo $get_post_info->description;} ?>
                          	
                   </textarea>
                  
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-4" style="border-left:1px solid black;">
                <div class="form-group">
                  <label>Category </label>
                  <select class="form-control select2" name="category_id" style="width: 100%;" required="required">
                  	
                  	<?php if(isset($category_list) && !empty($category_list)){
                  		foreach($category_list as $row){ ?>
                  		<option <?php if(isset($get_post_info) && $row->id == $get_post_info->category_id){?> selected="selected" <?php }?> value="<?php echo $row->id;?>"><?php echo $row->category_name;?></option>
                  	
                  	<?php	} } ?>
                    
                  </select>
                 
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Is Feature ?</label>
                  <select class="form-control select2" name="is_feature" style="width: 100%;">
                    <option <?php if(isset($get_post_info) && $get_post_info->is_feature == '0'){?> selected = "selected" <?php } ?> value ="0">No</option>
                    <option <?php if(isset($get_post_info) && $get_post_info->is_feature == '1'){?> selected = "selected" <?php } ?> value="1">Yes</option>
                    
                    
                  </select>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                  <label>Image</label>
                <input type="file" class="form-control"  name="image">

                <?php if(isset($get_post_info) && !empty($get_post_info->image)){?>
                	<img src="<?php echo base_url();?>assets/images/<?php echo $get_post_info->image;?>" width="50" height="50">

                 <?php }?>
                </div>

                 <div class="form-group text-center">
                 
                <input type="submit" class="btn btn-sm btn-primary"  value="Save">
                </div>
              </div>
           
              <!-- /.col -->
            </div>
            </form>
            <!-- /.row -->


          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about
            the plugin.
          </div>
        </div>
        <!-- /.card -->
          </div>
         
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
 <?php $this->load->view('Layouts/admin_footer');?>