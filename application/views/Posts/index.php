<?php $this->load->view('Layouts/admin_header');?>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row justify-content-center">
          <div class="col-12 col-sm-6 col-md-12">

           <div class="card">

           	 <div class="card-header">
                <h3 class="card-title">DataTable with default features</h3>
                <a href="<?php echo base_url();?>index.php/Posts/add" type="button" class="add_category btn btn-sm btn-primary" style="float: right;">Add</a>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <table id="cat_table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>SL</th>
                    <th>Image </th>
                    <th>Category Name</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Entry By </th>
                    <th>Action</th>
                    
                  </tr>
                  </thead>
                  <tbody>
                 <?php if(isset($post_list) && !empty($post_list)){ 
                 	$i=1;
                 	foreach($post_list as $list){ ?>

                  <tr>
                    <td><?php echo $i;?></td>
                    <td><img src="<?php echo base_url();?>assets/images/<?php echo $list->image;?>" width="50" height="50"></td>
                    <td><?php echo $list->category_name ;?></td>
                    <td><?php echo $list->title; ?></td>
                    <td><?php echo substr($list->description,0,70); ?></td>
                    <td><?php echo $list->user_name; ?></td>
                    <td>
                    	<a href="<?php echo base_url();?>index.php/Posts/edit/<?php echo $list->id;?>" class=" btn btn-info btn-xs" > Edit</a>
                    	<a href="<?php echo base_url();?>index.php/Posts/delete/<?php echo $list->id;?>/<?php echo $list->image;?>" class="btn btn-danger btn-xs"> Delete</a>
                    </td>
                  </tr>
                  
                  <?php $i++; } }?>
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
         
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
 <?php $this->load->view('Layouts/admin_footer');?>