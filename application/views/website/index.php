<?php $this->load->view('Layouts/website/header');?>

		<div class="container">
			<div id="breaking-news">
				<span>Breaking News</span>
				<div class="breaking-news-scroll">
					<ul>
						<?php if(isset($is_feature) && !empty($is_feature)){
						  foreach($is_feature as $feature){ ?>
						<li><i class="fa fa-angle-double-right"></i>
							<a href="<?php echo base_url();?>index.php/Homes/show_news_details/<?php echo $feature->id;?>" title=""><?php echo $feature->title;?></a>
						</li>
						
					<?php } } ?>
					</ul>
				</div>
			</div><!--#breaking-news-->
			
			<div class="section first_section">
				<div class="row">

					<div class="col-md-3">
                            <div class="widget stickybar">
                             
								<h1 class="block-title font-size"><span>OFFER</span></h1>

								<?php if(isset($middle_add) && !empty($middle_add)){ ?>
								<img class="img-responsive card-image" src="<?php echo base_url();?>assets/images/<?php echo $middle_add;?>" alt="" />
							<?php } ?>
								
										</div> 
                                
                            
                        </div><!--/#widget-->
					<div class="site-content col-md-6">
						<div class="row">
							<div class="col-sm-12">
								<div class="section photo-gallery">
											<h5 class="block-title font-size"><span>Lates News </span></h5>	
											<div id="photo-gallery" class="slideshow-list carousel slide carousel-fade post" data-ride="carousel">						
												<div class="carousel-inner">

												<?php if(isset($is_feature) && !empty($is_feature)){
													$i = 0;
													foreach($is_feature as $feature){ ?>

													<div class="item <?php if($i==0) { ?> active <?php } ?>">
														<a href="<?php echo base_url();?>index.php/Homes/show_news_details/<?php echo $feature->id;?>"><img class="img-responsive" src="<?php echo base_url();?>assets/images/<?php echo $feature->image;?>" alt="" width="850" height="350"/>
														</a>

														<h2><a href="<?php echo base_url();?>index.php/Homes/show_news_details/<?php echo $feature->id;?>"><?php echo $feature->title;?></a></h2>


													</div>

													<?php $i++; } } ?>	
													

													
													
												</div><!--/carousel-inner-->
												
												
												
												<div class="gallery-turner">
													<a class="left-photo" href="#photo-gallery" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
													<a class="right-photo" href="#photo-gallery" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
												</div>
											</div>
										</div><!--/photo-gallery--> 
							</div>
							
						</div>
						
					</div><!--/#content--> 
					
					<div class="col-md-3 col-sm-12">
						<div id="sitebarh" class="sitebar_width">
							
							<div class="widget">
								<h1 class="block-title font-size"><span>More Lates News</span></h1>
								<ul class="post-list">

									<?php if(isset($is_feature_other_list) && !empty($is_feature_other_list)){
										foreach($is_feature_other_list as $feature_list){ ?>

									<li>
										<div class="post small-post">
											<div class="entry-header">
												<div class="entry-thumbnail">
													<img class="img-responsive" src="<?php echo base_url();?>assets/images/<?php echo $feature_list->image;?>" alt=""  width="95" height="100"/>
												</div>
											</div>
											<div class="post-content">								
												<div class="video-catagory"><a class="title_font_size" href="<?php echo base_url();?>index.php/Homes/show_news_details/<?php echo $feature_list->id;?>"><?php echo $feature_list->title;?></a></div>
												<h2 class="entry-title ">
													<a href="" class="title_font_size_for_date"> <?php echo 'Published:'.$feature_list->created_at;?></a>
												</h2>
											</div>
										</div><!--/post--> 
									</li>

									<?php $i++; } } ?>	
									
											</ul>
										</div>
									
									</div>
								</div>
				</div>
			</div><!--/.section--> 


		



			
			<div class="inner-add">
				<a href="#">
					<?php if(isset($bottom_add) && !empty($bottom_add)){ ?>
								<img class="img-responsive" src="<?php echo base_url();?>assets/images/<?php echo $bottom_add;?>" alt="" />
							<?php } ?>
				</a>
			</div><!--/.section-->
			
		<!------end of slideshow section ------------------------->


			
			<div class="section second_section">
				<div class="row">
					<div class="col-md-9 col-sm-8">
						<div id="site-content">
							<div class="row ">
								<div class="col-md-12 col-sm-6">
									<div class="left-content">
									
										<div class="section health-section">
											<h1 class="block-title font-size"><span>International</span> </h1>
											<div class="row top_margin">
											<?php if(isset($international) && !empty($international)){
												foreach($international as $inter){ ?>
												<div class="col-md-6">
													<div class="health-feature">
												<div class="post international">
													<div class="entry-header">
														<div class="entry-thumbnail">
															<img class="img-responsive" src="<?php echo base_url();?>assets/images/<?php echo $inter->image;?>" alt="">
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo $inter->created_at;?>5 </a></li>
																
															</ul>
														</div>
														<h2 class="entry-title ">
															<a class="title_font_size" href="<?php echo base_url();?>index.php/Homes/show_news_details/<?php echo $inter->id;?>"><?php echo $inter->title;?></a>
														</h2>
													</div>
												</div><!--/post--> 
											</div>
												</div>

												<?php } } ?>



											</div>
										</div><!--/.lifestyle -->

									</div><!--/.left-content-->
								</div>
								
							</div>
						</div><!--/#site-content-->
					</div>
					<div class="col-md-3 col-sm-4">
						<div id="sitebarh" class="sitebar_width">
							
							<div class="widget">
								<h1 class="block-title font-size"><span>Bangladesh</span> </h1>
								<ul class="post-list">

									<?php if(isset($bangladesh) && !empty($bangladesh)){
										foreach($bangladesh as $bangla){ ?>

									<li>
										<div class="post small-post">
											<div class="entry-header">
												<div class="entry-thumbnail">
													<img class="img-responsive" src="<?php echo base_url();?>assets/images/<?php echo $bangla->image;?>" alt=""  width="95" height="100"/>
												</div>
											</div>
											<div class="post-content">								
												<div class="video-catagory"><a class="title_font_size" href="<?php echo base_url();?>index.php/Homes/show_news_details/<?php echo $bangla->id;?>"><?php echo $bangla->title;?></a></div>
												<h2 class="entry-title ">
													<a href="" class="title_font_size_for_date"> <?php echo 'Published:'.$bangla->created_at;?></a>
												</h2>
											</div>
										</div><!--/post--> 
									</li>

									<?php } } ?>	
									
											</ul>
										</div>
									
									</div>
								</div>
							</div><!--/#widget-->
							
						</div><!--/second section end-->



						<!------------------third section starts------------------------------------->
			
			<div class="section">
				<div class="row">
					<div class="col-md-9 col-sm-8">
						<div id="site-content">
							<div class="row">
								<div class="col-md-12 col-sm-6">
									<div class="left-content">
									
										<div class="section health-section">
											<h1 class="block-title font-size"><span>Sports</span> </h1>
											<div class="row top_margin">
											<?php if(isset($sports) && !empty($sports)){
												foreach($sports as $sport){ ?>
												<div class="col-md-6">
													<div class="health-feature">
												<div class="post international">
													<div class="entry-header">
														<div class="entry-thumbnail">
															<img class="img-responsive" src="<?php echo base_url();?>assets/images/<?php echo $sport->image;?>" alt="">
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo $sport->created_at;?>5 </a></li>
																
															</ul>
														</div>
														<h2 class="entry-title ">
															<a class="title_font_size" href="<?php echo base_url();?>index.php/Homes/show_news_details/<?php echo $sport->id;?>"><?php echo $sport->title;?></a>
														</h2>
													</div>
												</div><!--/post--> 
											</div>
												</div>

												<?php } } ?>



											</div>
										</div><!--/.lifestyle -->

									</div><!--/.left-content-->
								</div>
								
							</div>
						</div><!--/#site-content-->
					</div>
					<div class="col-md-3 col-sm-4">
						<div id="sitebarh" class="sitebar_width">
							
							<div class="widget">
								<h1 class="block-title font-size"><span>Politics</span>  </h1>
								<ul class="post-list">

									<?php if(isset($politics) && !empty($politics)){
										foreach($politics as $politic){ ?>

									<li>
										<div class="post small-post">
											<div class="entry-header">
												<div class="entry-thumbnail">
													<img class="img-responsive" src="<?php echo base_url();?>assets/images/<?php echo $politic->image;?>" alt=""  width="95" height="100"/>
												</div>
											</div>
											<div class="post-content">								
												<div class="video-catagory"><a class="title_font_size" href="<?php echo base_url();?>index.php/Homes/show_news_details/<?php echo $politic->id;?>"><?php echo $politic->title;?></a></div>
												<h2 class="entry-title ">
													<a href="" class="title_font_size_for_date"> <?php echo 'Published:'.$politic->created_at;?></a>
												</h2>
											</div>
										</div><!--/post--> 
									</li>

									<?php } } ?>	
									
											</ul>
										</div>
									
									</div>
								</div>
							</div><!--/#widget-->
							
						</div><!--/second section end-->
						<!------------------third section end------------------------------------->
					

					</div>
				</div>				
			</div><!--/.section-->
		</div><!--/.container-->
		
	
		
<?php $this->load->view('Layouts/website/footer');?>