<?php $this->load->view('Layouts/website/header');?>

		<div class="container min_height">
			

						<!------------------third section starts------------------------------------->

				<div class="section first_section">
				<div class="row">

					<div class="col-md-3 col-sm-4">
						<div id="sitebar">
							
							<div class="widget">
								<h1 class="section-title title font-size">Reporter </h1>
								<ul class="post-list">
									<div class="entry-meta">
														<ul class="list-inline">
															<li class="posted-by"><i class="fa fa-user"></i> by <a ><?php if(isset($specific_post)){ echo $specific_post->user_name;}?></a>
															</li><br>
															<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php if(isset($specific_post)){ echo 'Published on: '. $specific_post->created_at;} ?> </a></li>
															
														</ul>
													</div>
									
											</ul>
										</div>
									
									</div>
								</div>


					<div class="col-md-6 col-sm-8">
						<div id="site-content">
							<div class="row">
								<div class="col-md-12 col-sm-6">
									<div class="left-content">
									
										<div class="section lifestyle-section">
											<h1 class="block-title font-size"><span><?php if(isset($category_name)){ echo $category_name;}?></span></h1>
											<div class="row slideshow-list">
											<?php if(isset($specific_post) && !empty($specific_post)){ ?>

												<div class="col-md-12 specific_post_image">
													<div class="post medium-post specific_post">
														<div class="entry-header">
															<div class="entry-thumbnail">
																<img class="img-responsive" src="<?php echo base_url();?>assets/images/<?php echo $specific_post->image;?>" alt="" />
															</div>
														</div>
														<div class="post-content">								
													
													<h2 class="entry-title">
														<?php echo $specific_post->title;?>
													</h2>
													<div class="entry-content">
														<p>
															<?php echo $specific_post->description;?>
														</p>
														
													
													</div>
												</div>
													</div><!--/post--> 
													
												</div>

											<?php } ?>
												
												

											</div>
										</div><!--/.lifestyle -->

									</div><!--/.left-content-->
								</div>
								
							</div>
						</div><!--/#site-content-->
					</div>
					<div class="col-md-3 col-sm-4">
						<div id="sitebar" class="sitebar_width">
							
							<div class="widget">
								<h1 class="block-title font-size"><span><?php if(isset($category_name)){ echo $category_name;}?></span></h1>
								<ul class="post-list">
									<?php if(isset($category_wise_post) && !empty($category_wise_post)){
										foreach($category_wise_post as $post) { ?>
										<li>
										<div class="post small-post">
											<div class="entry-header">
												<div class="entry-thumbnail">
													<img class="img-responsive" src="<?php echo base_url();?>assets/images/<?php echo $post->image;?>" alt="" />
												</div>
											</div>
											<div class="post-content">								
												<div class="video-catagory"><a href="<?php echo base_url();?>index.php/Homes/show_news_details/<?php echo $post->id;?>"><?php echo $post->title;?></a></div>
												<h2 class="entry-title">
													<a href=""><?php echo 'Published: '.$post->created_at;?></a>
												</h2>
											</div>
										</div><!--/post--> 
									</li>
										<?php } } ?>
									
									
											</ul>
										</div>
									
									</div>
								</div>
							</div><!--/#widget-->
							
						</div><!--/second section end-->
						<!------------------third section end------------------------------------->
					

					</div>
				</div>				
			</div><!--/.section-->
		</div><!--/.container-->
		
	
		
<?php $this->load->view('Layouts/website/footer');?>