<?php $this->load->view('Layouts/website/header');?>

		<div class="container min_height">
			<div id="breaking-news">
				<span>Breaking News</span>
				<div class="breaking-news-scroll">
					<ul>
						<?php if(isset($is_feature) && !empty($is_feature)){
						  foreach($is_feature as $feature){ ?>
						<li><i class="fa fa-angle-double-right"></i>
							<a href="<?php echo base_url();?>index.php/Homes/show_news_details/<?php echo $feature->id;?>" title=""><?php echo $feature->title;?></a>
						</li>
						
					<?php } } ?>
					</ul>
				</div>
			</div><!--#breaking-news-->

						<!------------------third section starts------------------------------------->

				<div class="section first_section ">
				<div class="row">

					<div class="col-md-9 col-sm-8">
						<div id="site-content">
							<div class="row">
								<div class="col-md-12 col-sm-6">
									<div class="left-content">
									
										<div class="section health-section">
											<h1 class="block-title font-size"><span><?php if(isset($category_name)){ echo $category_name;}?></span></h1>
											<div class="row">
											<?php if(isset($post_list) && !empty($post_list)){
												foreach($post_list as $post){ ?>
												<div class="col-md-6">
													<div class="health-feature">
												<div class="post international">
													<div class="entry-header">
														<div class="entry-thumbnail">
															<img class="img-responsive" src="<?php echo base_url();?>assets/images/<?php echo $post->image;?>" alt="">
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo $post->created_at;?> </a></li>
																
															</ul>
														</div>
														<h2 class="entry-title ">
															<a class="title_font_size" href="<?php echo base_url();?>index.php/Homes/show_news_details/<?php echo $post->id;?>"><?php echo $post->title;?></a>
														</h2>
													</div>
												</div><!--/post--> 
											</div>
												</div>

												<?php } } ?>



											</div>
										</div><!--/.lifestyle -->

									</div><!--/.left-content-->
								</div>
								
							</div>
						</div><!--/#site-content-->
					</div>




				
					<div class="col-md-3 col-sm-4">
						<div id="sitebar">
							
							<div class="widget">
								<h1 class="block-title font-size"><span><?php if(isset($category_name)){ echo 'More  '.$category_name. ' News';}?></span></h1>
								<ul class="post-list">
								<?php if(isset($post_other_list) && !empty($post_other_list)){
												foreach($post_other_list as $post_other){ ?>
									<li>
										<div class="post small-post">
											<div class="entry-header">
												<div class="entry-thumbnail">
													<img class="img-responsive" src="<?php echo base_url();?>assets/images/<?php echo $post_other->image;?>" alt="" />
												</div>
											</div>
											<div class="post-content">								
												<div class="video-catagory"><a href="<?php echo base_url();?>index.php/Homes/show_news_details/<?php echo $post_other->id;?>"><?php echo $post_other->title;?></a></div>
												<h2 class="entry-title">
													<a href=""><?php echo $post_other->created_at;?></a>
												</h2>
											</div>
										</div><!--/post--> 
									</li>

									<?php } } ?>
									
											</ul>
										</div>
									
									</div>
								</div>
							</div><!--/#widget-->
							
						</div><!--/second section end-->
						<!------------------third section end------------------------------------->
					

					</div>
				</div>				
			</div><!--/.section-->
		</div><!--/.container-->
		
	
		
<?php $this->load->view('Layouts/website/footer');?>