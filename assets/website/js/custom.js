jQuery(function ($) {

    'use strict';
	
	/*==============================================================*/
    // Table of index
    /*==============================================================*/

    /*==============================================================
    # sticky-nav
    # Date Time
    # language Select
	# Search Slide
	# Breaking News
	# Owl Carousel
	# magnificPopup
	# newsletter
	# weather	
	
    ==============================================================*/
	
	
	
	
	/*==============================================================*/
    // # sticky-nav
    /*==============================================================*/
		$(document).ready(function () { 
			//Disable cut copy paste 
			$('body').bind('cut copy paste', function (e) { 
		        e.preventDefault(); 
		    }); 
		    //Disable full page 
		    $("body").on("contextmenu",function(e){ 
		        return false; 
		    }); 

			document.onkeydown = function(e) {
	        if (e.ctrlKey && 
	             
	            (
	             e.keyCode === 85 
	             )) {
	            alert('Disabled!');
	            return false;
	        } else if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
				 alert('Disabled!');
				 return false;
			}else {
	            return true;
			}

		  };
		     
		   




            if($(document).scrollTop()>100) {
                
                $(".stickybar").css("top","10%");
                $(".stickybar_details").css("top","10%");
            } else {
                $(".stickybar").css("top","22%");
                $(".stickybar_details").css("top","34%");
            }
            
        });
	
	   $(window).scroll(() => { 
  // Distance from top of document to top of footer.
  topOfFooter = $('#footer').position().top;
  // Distance user has scrolled from top, adjusted to take in height of sidebar (570 pixels inc. padding).
  scrollDistanceFromTopOfDoc = $(document).scrollTop() + 570;
  // Difference between the two.
  scrollDistanceFromTopOfFooter = scrollDistanceFromTopOfDoc - topOfFooter;

  // If user has scrolled further than footer,
  // pull sidebar up using a negative margin.
  if (scrollDistanceFromTopOfDoc > topOfFooter) {
    $('.stickybar').css('margin-top',  0 - scrollDistanceFromTopOfFooter);
    $('.stickybar').css('z-index', 0);
  } else  {
    $('.stickybar').css('margin-top', 0);
  }
});
   

    </script>