-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2020 at 08:27 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newsportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`) VALUES
(4, 'International'),
(5, 'Entertainment'),
(6, 'Bangladesh'),
(7, 'Sports'),
(8, 'Politics'),
(9, 'Literature'),
(10, 'Editorial'),
(11, 'Technology'),
(12, 'Health'),
(13, 'Business');

-- --------------------------------------------------------

--
-- Table structure for table `logos`
--

CREATE TABLE `logos` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '0=>add,1=>logo',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT '0=>no position, 1=> top, 2=>middle,3=>bottom',
  `user_id` int(11) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logos`
--

INSERT INTO `logos` (`id`, `image`, `type`, `position`, `user_id`, `created_at`, `updated_at`) VALUES
(2, '19464222555f4c970d4c766.jpg', 0, 1, 1, '2020-08-31', '2020-08-31'),
(3, '2147469675f4c7c1929428.png', 1, 0, 1, '2020-08-31', NULL),
(4, '6406963305f4c969d9f1d9.jpg', 0, 2, 1, '2020-08-31', '2020-08-31');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `is_feature` int(11) NOT NULL DEFAULT '0' COMMENT '0=>no, 1=>yes',
  `created_at` date NOT NULL,
  `updated_at` date DEFAULT NULL,
  `entry_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `category_id`, `title`, `description`, `image`, `is_feature`, `created_at`, `updated_at`, `entry_by`) VALUES
(5, 4, 'this is title', 'this is titlethis is titlethis is titlethis is titlethis is titlethis is titlethis is title', '3405410875f4a8a37a254d.png', 0, '2020-08-29', '2020-08-29', 1),
(6, 6, 'SDFASFAS', 'SDFASDFSADFSADF', NULL, 0, '0000-00-00', '2020-08-29', 1),
(7, 6, 'ASFASDFSA', 'ASDFASDFSADFSAFSAFSAFSFSADF', NULL, 0, '0000-00-00', '2020-08-29', 1),
(8, 4, 'Restaurant collapse in China\'s Shanxi kills 29', '<p><span style=\"font-size: 14px;\">Twenty-nine people were killed and \r\nseven seriously injured when a restaurant collapsed in northern China\'s \r\nShanxi province, the country\'s emergencies ministry media said on \r\nSunday<br></span></p><p><span style=\"font-size: 14px;\">The\r\n building collapsed at 9:40 a.m. (0140 GMT) on Saturday in Xiangfen \r\ncounty in the southwest of Shanxi, the Ministry of Emergency Management \r\nsaid in a statement, reports Reuters.</span></p><p><span style=\"font-size: 14px;\">The\r\n accident in the two-storey structure occurred as villagers and \r\nrelatives gathered for a birthday party, and the rescue operation ended \r\nearly on Sunday, state media said.</span></p><p><span style=\"font-size: 14px;\">Fifty-seven people were pulled from the debris, with 29 confirmed dead, while 21 suffered minor injuries.</span></p><p><span style=\"font-size: 14px;\">The\r\n Shanxi provincial government has set up a high-level team to \r\ninvestigate the accident in the county, which is under the jurisdiction \r\nof the city of Linfen, the emergency management ministry said.</span></p>', '2614774475f4b96c0c7056.jpg', 1, '2020-08-30', '2020-08-30', 1),
(9, 4, 'World\'s highest record 78,761 new virus cases in a day in India', 'India on Sunday set a new virus record when it reported 78,761 new \r\ninfections in 24 hours, according to health ministry figures, passing \r\nthe United States for the world’s highest single-day rise.<br>\r\n<br>\r\nIndia, home to 1.3 billion people, is already the world’s third-most \r\ninfected nation with more than 3.5 million cases, behind the US and \r\nBrazil, reports AFP.<br>\r\n<br>\r\nIt has also reported more than 63,000 deaths.<br>\r\n<br>\r\nThe US set the previous record on July 17 with 77,638 daily infections, according to an AFP tally.<br>\r\n<br>\r\nIndia’s grim milestone came a day after the government further eased its\r\n coronavirus lockdown, in place since late March, to boost the \r\nstruggling economy.<br>\r\n<br>\r\nMillions have lost their jobs since the start of the lockdown, with the poor particularly hard hit.<br>\r\n<br>\r\nThe Home Affairs Ministry said gatherings of up to 100 people would be \r\nallowed with face masks and social distancing at cultural, \r\nentertainment, sports and political events from next month.<br>\r\n<br>\r\nMetro train services would also resume “in a graded manner” in major cities.<br>\r\n<br>\r\nThe coronavirus has badly hit megacities such as financial hub Mumbai \r\nand the capital New Delhi, but is now also surging in smaller cities and\r\n rural areas.<br>\r\n<br>\r\nSchools remain closed but students can meet teachers on a voluntary \r\nbasis on school premises if needed, according to the new guidelines.', '16114964135f4b97116e096.jpg', 1, '2020-08-30', '2020-08-30', 1),
(10, 4, 'Trump to visit Kenosha in wake of racial unrest', '<p><span style=\"font-size: 14px;\">Trump will meet police in Kenosha, \r\nWisconsin on Tuesday and “survey damage from recent riots” triggered by \r\nBlake’s shooting last weekend, White House spokesman Judd Deere said \r\nSaturday, reports AFP.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Blake\r\n took at least half a dozen shots in front of his small children as he \r\ntried to get into a car, in an incident that has prompted an outpouring \r\nof anger over yet another shooting of a black man by police.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Deere did not say if Trump would meet the family of Blake, 29, who was left paralyzed from the waist down.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Protesters\r\n have taken to the streets in major cities nationwide this summer over \r\nthe deaths of black people at the hands of police, including George \r\nFloyd in Minneapolis in May.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">It is the most widespread civil unrest in the United States for decades.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Trump\r\n has characterized the mostly-peaceful activists as rioters as he pushes\r\n a law and order message while fighting an uphill battle for re-election\r\n in November.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Kenosha,\r\n about an hour’s drive from Chicago, saw three nights of violence after \r\nthe Blake shooting as protesters set fire to buildings and cars.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Major\r\n US sports leagues including the NBA were forced to suspend play as \r\nAfrican American and other players outraged by the shooting joined the \r\nlatest national wave of anger over racial injustice and police \r\nmisconduct.</span></p>', '15758915075f4b9758ee950.jpg', 0, '0000-00-00', '2020-08-30', 1),
(11, 6, '1,897 new virus cases, 42 deaths', '<p><span style=\"font-size: 14px;\">?Bangladesh has recorded 42 new fatalities from the novel coronavirus infection in a day, raising the death toll to 4,248.<br></span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">With new 1,897 confirmed cases of COVID-19 on Sunday, t</span><span style=\"font-size: 14px;\">he tally of infections jumped to 310,822.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">According to data released by the health directorate,&nbsp;</span><span style=\"font-size: 14px;\">another 3,044 patients got recovery from the disease through treatment at home and in hospital care.</span></p>', '11546427365f4b97a722ac4.jpg', 1, '2020-08-30', '2020-08-30', 1),
(12, 6, 'Holy Ashura being observed', '<span style=\"font-size: 14px;\">The holy Ashura, commemorating the \r\nmartyrdom of Hazrat Imam Hossain Ibn Ali (RA), a grandson of Prophet \r\nHazrat Muhammad (Peace Be Upon Him), is being observed across the \r\ncountry on Sunday with due religious solemnity.<br></span><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Today is the 10th day of the month of Muharram in Hijri calendar. </span></p><p><span style=\"font-size: 14px;\">Muslims\r\n across the world recall the sacrifice of Hazrat Imam Hossain (RA) along\r\n with his family members and 72 followers, who embraced martyrdom in 680\r\n AD for establishing truth and justice in the fight with soldiers of \r\nYazid on Karbala Maidan in Iraq.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">President\r\n Md Abdul Hamid and Prime Minister Sheikh Hasina have issued separate \r\nmessages on the eve of the holy Ashura, paying deep respect to Hazrat \r\nImam Hossain (RA) and other martyrs of Karbala.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">In his message, the President said, “The Holy Ashura is a significant and mourning day for the whole Muslim Ummah.”</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">On\r\n Muharram 10 in Hijri 61, Hazrat Imam Hossain (RA), grandson of Prophet \r\nHazrat Mohammad (PBUH), his family members and his companions embraced \r\nmartyrdom for establishing truth and justice in the fight with soldiers \r\nof Yazid on Karbala Maidan, he added.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Their supreme sacrifice for upholding the ideals of Islam remains as a glorious event in the history, Hamid said.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">He\r\n said the tragic incident of Karbala inspires “us” to be vocal against \r\nany injustice and repression and lead the life in the path of truth and \r\nbeauty.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">“Islam is a religion of peace and harmony. Here there is no room for confrontation, jealousy and envy,” the President added.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">The\r\n President hoped that the lesson of Ashura would act as the source of \r\ninspiration for all to spread the light of truth and beauty in the \r\nsociety along with establishing the social and religious values.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">The\r\n Prime Minister, in her message, said the sacrifice of Hazrat Imam \r\nHossain (RA) and his companions to establish truth and justice in \r\nsociety is a glaring and emulating example for the Muslim Ummah across \r\nthe world.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">“We,\r\n however, are observing the Ashura this year in a crisis period. The \r\ncoronavirus has paralyzed the whole world. Our government is taking all \r\nnecessary steps to face the situation. We continue all sorts of support \r\nto the people,” Sheikh Hasina said.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Allah\r\n tests the patience of people when they are in trouble, the premier \r\nsaid, adding that in this time everyone should come forward to help each\r\n other with patience and tolerance.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">She urged all to follow health guidelines and offer special prayers to the Almighty Allah seeking relief from the lethal virus.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">“Let’s\r\n build a discrimination-free, happy, prosperous and peaceful Bangladesh \r\nbeing imbued with the spirit of the holy Ashura by participating in \r\nwelfare-oriented works from our respective positions in establishing \r\ntruth and justice at the national level,” the Prime Minister said.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">During\r\n the ongoing coronavirus crisis, a short programme has been taken all \r\nover the country, including Dhaka, on this occasion. Police have taken \r\nspecial security measures in the capital marking the occasion.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">The\r\n Dhaka Metropolitan Police (DMP) has banned all kinds of tazia, mourning\r\n and pike processions in the Dhaka metropolitan area on the occasion of \r\nthe holy Ashura.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">However,\r\n devoted city dwellers will be able to perform religious rite in \r\ncompliance with the health rules. But, carrying of knives, daggers, \r\nscissors, spears, swords, sticks and displaying of fireworks and \r\ncrackers are strictly prohibited in these programmes.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">The\r\n DMP urged not to allow anybody to enter the function venue without \r\nwearing masks and asked everyone to apply necessary health protective \r\nmeasures at all stages.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">The day is a public holiday.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Bangladesh\r\n Betar, Bangladesh Television and private TV channels are airing special\r\n programmes while newspapers and online news portals have published \r\nsupplements highlighting the significance of the day.</span></p>', '13455321715f4b97e07f08a.jpg', 1, '0000-00-00', '2020-08-30', 1),
(13, 8, 'Six-Point Demand only Bangabandhu’s brainchild: PM', '<p><span style=\"font-size: 14px;\">?Prime Minister Sheikh Hasina on \r\nWednesday said the historic Six-Point Demand was solely the brainchild \r\nof Father of the Nation Bangabandhu Sheikh Mujibur Rahman as no-one else\r\n was involved in formulating this crucial political programme that led \r\nthe county to its Independence.<br></span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">\"Many\r\n people want to say many things regarding the Six-Point Demand…some say \r\nit was done by that person\'s suggestions, some says it was formulated \r\nwith advice from those persons, but, I know for sure, it was the \r\nbrainchild of his (Bangabandhu Sheikh Mujibur Rahman) own thinking,\" she\r\n said, reports UNB.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">The\r\n Prime Minister said this while distributing awards among the winners of\r\n quiz competition, which was organised marking the historic Six-Point \r\nDemand (June 7).</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Father\r\n of the Nation Bangabandhu Sheikh Mujibur Rahman Birth Centenary \r\nCelebration National Implementation Committee organised the programme at\r\n International Mother Language Institute in the capital.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">The Prime Minister joined the programme from her official residence Ganobhaban virtually.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Recalling\r\n the background of this historical demands of the country, she said \r\nBangabandhu was arrested in 1958 and released in December 17, 1969. At \r\nthat time, the politics was banned. Bangabandhu could not go outside \r\nDhaka, and he joined Alfa Insurance Company.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">She\r\n mentioned that Tajuddin Ahmed was also under arrest at that time. After\r\n release, he went to Fatulla, Narayanganj to join his job. Bangabandhu \r\nhimself went to Narayanganj and bring Tajuddin Ahmed back to Dhaka and \r\ngave a job in Alfa Insurance Company.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Sheikh\r\n Hasina, the eldest daughter of Bangabandhu Sheikh Mujibur Rahman who \r\nwas studying at Dhaka University then, said the Father of the Nation \r\nappointed Mohammad Hanif as his PA in the Alfa Insurance Company.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">\"Bangabandhu\r\n used to think, write down those and gave those write-ups to Hanif for \r\ntyping out. Here only Hanif knew about this (Six-Point Demand) as he \r\ntyped out that …no-one else would know about it,\" she said.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">The\r\n Prime Minister categorically said the Six-Point Demand came out from \r\nthe thoughts of Bangabandhu after the 1965 India-Pakistan war that \r\nlasted for three weeks and the then East Pakistan (now Bangladesh) was \r\ntotally left defenceless.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">She\r\n also said after the arrest of Bangabandhu and other front-ranking AL \r\nleaders due to the announcement of the Six- Point Demand, Begum \r\nFazilatunnesa Mujib (mother of Prime Minister Sheikh Hasina) had played a\r\n great role in publicising and advancing the movement centring these \r\ndemands.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">\"It\r\n cannot be imagined the contributions of my mother to Bangladesh, she \r\nalways knew what my father wanted, she was very aware about those,\" \r\nHasina said.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">The Prime Minister said the August-15, 1975 massacre stopped the advancement of Bangladesh.</span></p>', '9441827545f4b9d49e7c95.jpg', 0, '0000-00-00', '2020-08-30', 1),
(14, 8, 'EC’s proposal for scrapping power to cancel candidature \'suicidal\': BNP', '<span style=\"font-size: 14px;\">BNP on Friday strongly opposed the \r\nElection Commission’s (EC’s) proposal for scrapping its direct power to \r\ncancel the candidature of candidates by amending the Representation of \r\nthe People Order (RPO), saying it is a \'suicidal\' move.<br></span><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">“The\r\n Election Commission is an independent and sovereign entity in a \r\ndemocratic system. As per section 91(E) of the RPO, it’s the \r\njurisdiction of the Election Commission to cancel the candidature of any\r\n candidate. The Constitution has given it this power and freedom,” said \r\nBNP senior joint secretary general Ruhul Kabir Rizvi.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">He\r\n said, \"Does anyone curtail his/her freedom? But the Election Commission\r\n is now voluntarily surrendered its independence and such power to the \r\ngovernment. They themselves are taking a suicidal decision.”</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">He\r\n came up with the remarks at a milad mahfil organised by the Rajshahi \r\nUniversity Nationalist Ex-Student Association (Runesa) at BNP’s \r\nNayapaltan central office, seeking salvation of the departed soul of \r\nDilruba Shawkat, wife of BNP leader Shahin Shawkat.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Recently,\r\n the Commission has prepared a draft of the Representation of the People\r\n Act-2020. The draft proposal containing 34 recommendations, including \r\nabolishing the EC’s direct power to cancel a candidate\'s candidature, \r\nwas finalised at a meeting of the commission on Wednesday.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Rizvi\r\n said the EC has adopted a resolution to curtail its power and make a \r\nseparate law over the annulment of candidature as per the government’s \r\ndirective.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">\"The\r\n government is going to keep the Election Commission’s power in its own \r\nhands by formulating a law. That law will help Sheikh Hasina hold \r\nabsolute power. If anyone disobeys the government, will be excluded. If \r\nBNP and other parties raise, voice their registrations will be revoked \r\nsince the law is in the hands of Sheikh Hasina. And this power is being \r\nhanded over to the government by the Chief Election Commissioner and \r\nsome election commissioners. Only a commissioner gave a note of \r\ndissent,” he said.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">The\r\n BNP leader said this move has been taken to restore a one-party Baksal \r\nrule in a new style by snatching the rights of other parties to do \r\npolitics. \"That means Sheikh Hasina is the unchallenged \'lord\', and If \r\nanyone raises voice against what she says or does will face dire \r\nconsequences. This is Baksal under a new cover.”</span></p>', '3834252205f4b9d8161c7b.jpeg', 0, '0000-00-00', '2020-08-30', 1),
(15, 8, 'Intruders tarnish image of AL, govt: Quader', '<p><span style=\"font-size: 14px;\">?Awami League General Secretary and \r\nRoad Transport and Bridges Minister Obaidul Quader today said intruders \r\nand opportunists are tarnishing the image of the ruling party as well as\r\n the government.<br></span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">“Tested\r\n leaders and workers who stay with the party during its crisis never \r\ntarnish the image of the party or the government, but the intruders are \r\ndoing that,” he said, addressing a function to handover health \r\nprotective equipment to director general of DGHS under the arrangement \r\nof AL’s health and population affairs sub-committee at the party \r\npresident’s Dhanmondi political office here.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">He joined the function virtually from his official residence on parliament premises.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">The\r\n AL general secretary alerted the party leaders and workers so that no \r\nopportunist or intruder gets shelter or patronization at any level in \r\nthe party by any means.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Turning\r\n to the coronavirus (Covid-19) situation, Quader said Prime Minister \r\nSheikh Hasina’s government has been taking up effective measures since \r\nthe outset of the outbreak of the disease in the country.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Because\r\n of the premier’s able leadership and constant supervision, the \r\ngovernment has been able to keep the situation comparatively under \r\ncontrol, he added, reports BSS.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">He\r\n said the outbreak of the deadly coronavirus appeared as a completely \r\nnew experience in the medical science as it had no previous case \r\nhistory.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">But,\r\n the government’s sincere efforts are gradually enhancing the capability\r\n of the country’s health sector to deal with the novel coronavirus side \r\nby side raising awareness among the people to protect themselves from \r\nbeing infected by the virus, he added.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">The\r\n minister said sufficient allocations of necessary equipment and other \r\nthings related to Covid-19 treatment are being made while scopes of \r\ntreatment are increasing day by day in the privet sector as well.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Quader\r\n underscored the need for reducing the tendency of going abroad for \r\nmedical purposes and said the specialized hospitals should be turned \r\ninto center of excellence.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Mentioning\r\n that the government is making its all-out strides to reach quality \r\nmedical facilities at grassroots, he said if the physicians abide by the\r\n premier’s directives about concentrating on providing treatment at the \r\nvillage level by staying there, the quality of rural health services \r\nwill enhance further.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">The\r\n government wants to expand the health system at grassroots without \r\nkeeping it only capital-centric while some measures have been taken in \r\nthe recent time to enhance skills and remove irregularities in the \r\nsector, he added.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">He\r\n said making sincere efforts by all stakeholders in the sector is a must\r\n to take ahead the government initiatives as strong teamwork should be \r\nensured with the united and coordinated efforts of physicians, nurses, \r\nhealth workers and technologist to have a strong health system.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">The\r\n AL general secretary laid emphasis on increasing the number of Covid-19\r\n test and quick delivery of reports to lessen the hassle of people, \r\nspecially the abroad goers including the expatriate Bangladeshi workers.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">AL\r\n Presidium Member Begum Matia Chowdhury, Organizing Secretary SM Kamal \r\nHossain, Health Affairs Secretary Dr Rokeya Sultana, Relief and Social \r\nWelfare Secretary Sujit Roy Nandi, Office Secretary Barrister Biplab \r\nBarua and Deputy Office Secretary Sayem Khan were present among others.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Under\r\n the arrangement of the AL’s health and population affairs \r\nsub-committee, health protective equipment was handed over to \r\nDirectorate General of Health Services (DGHS) Director General Prof. Dr.\r\n Abul Bashar Mohammad Khurshid Alam on the occasion.</span></p>', '4179128545f4b9dadd86d1.jpg', 0, '0000-00-00', '2020-08-30', 1),
(16, 7, 'Tottenham sign £15m Doherty from Wolves', '<p><span style=\"font-size: 14px;\">?Tottenham Hotspur have signed defender Matt Doherty from Premier League rivals Wolverhampton Wanderers for a fee of about £15m.<br></span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">The Republic of Ireland right-back, 28, has signed a four-year deal with Spurs.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Doherty made over 300 appearances for Wolves after joining from League of Ireland side Bohemians in 2010.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">He\r\n becomes Spurs\' third signing of the summer following the arrivals of \r\nmidfielder Pierre-Emile Hojbjerg and goalkeeper Joe Hart.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Doherty\r\n played in every Premier League game of the 2018-19 campaign and helped \r\nNuno Espirito Santo\'s side to back-to-back seventh-place finishes since \r\npromotion from the Championship, as well as the Europa League \r\nquarter-finals last season.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Find all the latest football transfers on our dedicated page.</span></p>', '10154987325f4b9de5c36d6.jpg', 1, '0000-00-00', '2020-08-30', 1),
(17, 7, 'Messi skips Barca\'s pre-season medical', '<p><span style=\"font-size: 14px;\">Reuters footage showed players \r\narriving for coronavirus tests on Sunday morning and Messi did not \r\nappear. He had been due at Barcelona\'s training ground at 10.15am local \r\ntime. Players had been given staggered appointments throughout the \r\nmorning until midday.</span></p><p><span style=\"font-size: 14px;\">Media reports on Saturday said Argentine Messi would not undergo a pre-season medical or attend training on Monday.</span></p><p><span style=\"font-size: 14px;\">The\r\n 33-year-old six-times world player of the year informed his lifelong \r\nclub on Tuesday he wished to leave immediately, plunging Barca into new \r\nturmoil less than two weeks after their humiliating 8-2 Champions League\r\n quarter-final defeat by Bayern Munich.</span></p><p><span style=\"font-size: 14px;\">Messi\'s\r\n lawyers plan to invoke a clause in his four-year contract, signed in \r\n2017, which would have allowed the forward to leave the club for free if\r\n he had requested it by June 10.</span></p><p><span style=\"font-size: 14px;\">They\r\n will argue that that date - nominally the end of the season - is now \r\nirrelevant after the coronavirus delays that led to the season\'s \r\nextension and the team playing deep into August.</span></p><p><span style=\"font-size: 14px;\"><br></span></p><p><span style=\"font-size: 14px;\">Under\r\n the terms of the contract which expires in 2021, the only way Messi can\r\n leave without the club\'s consent is if another club pays his release \r\nclause of 700 million euros <br></span></p>', '20034077125f4bb8c5d4b4c.jpg', 1, '2020-08-30', '2020-08-30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active',
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_name`, `email`, `password`, `status`, `role_id`) VALUES
(1, 'sham', 'sham@gmail.com', '1', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logos`
--
ALTER TABLE `logos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `logos`
--
ALTER TABLE `logos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
